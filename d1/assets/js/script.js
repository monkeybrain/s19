//ES6 - ECMA Script is a standard for scripting languages like JavaScript

//ES5 - current version implemented into most browsers

//Destructuring -allows us to break apart key structure into variable or Array Destructuring

let employee = ['Sotto', 'Tito', 'Senate Dog', 'Apologist'];

let [lastName, firstName, position, gender] = employee;

console.log(firstName);
console.log(position);

let bulls = ['Caruso', 'Ball', 'Lavine', 'Derozan', 'Vucevic']

const [PG, SG, SF, PF, C] = bulls

console.log('PG: ' + PG);
console.log('SG: ' + SG);
console.log('SF: ' + SF);
console.log('PF: ' + PF);
console.log('C: ' + C);

//object destructuring- it allows us to destructure an object by allowing to add the values of an object's property into respective variables




getPet({
	species: 'god',
	color: 'white',
	breed: 'Bully'
});

/*let {breed, color} = pet;
alert(`I have a ${breed} pet and its color ${color}`)*/

/*function getPet(options){
	alert(`I have a ${options.breed} pet and its color ${options.color}`);
};*/


function getPet(options){
	let breed = options.breed;
	let color = options.color;

	console.log(`I have ${breed} dog and its color ${color}`)}


getPerson({
	personName: 'Marvin',
	personAge: '29',
	personBday: 'May 6, 1992'
})

function getPerson(options){
	let sentence1 = `Hi I am ${options.personName}`;
	let sentence2 = `I was born on ${options.personBday}`;
	let sentence3 = `I am ${options.personAge}`;
alert(sentence1)
alert(sentence2)
alert(sentence3)
}	

	Create 3 new sentence variable which will contain the following strings:
		sentence1: Hi, I'm <personName>,
		sentence2: I was born on <personbday>,
		sentence3: I am <personage> years old

	Display the three sentences in console or in alert
 */

let person = {
		name: 'Paul Phoenix',
		birthday: 'August 5, 1995',
		age: 26
	};
let sentence1 = `Hi, I'm ${person.name}`;
let sentence2 =`I was born on ${person.birthday}`;
let sentence3 =`I am ${person.age} years old.`;

console.log(sentence1);
console.log(sentence2);
console.log(sentence3);

const {age, name, birthday} = person;

console.log(age);
console.log(name);
console.log(birthday);

let pokemon1 = {
	name1: 'Charmander',
	level: 15,
	type: 'Fire',
	moves: ['ember', 'scratch','leer']
}

const {name1, level, type, moves} = pokemon1;
let sentence4 = `My pokemon is ${name1}. It is level ${level}. It is a ${type} and its movements are ${moves}.`

// what data type is moves?
console.log(moves);

const [move1,,move3] = moves;
console.log(move1);
console.log(move3);

pokemon1.name1 = 'Bulbasaur';
console.log(name1);
console.log(pokemon1);


// Arrow Functions - fat arrow
// before ES6
// function printFullName (firstName, middleInitial, lastName){
// 	return firstName + ' ' + middleInitial + ' ' + lastName;
// };

// // In ES6
// const printFullName = (firstName, middleInitial, lastName) => {
// 	return firstName + ' ' + middleInitial + ' ' + lastName;
// };

// traditional function
function displayMsg(){
	console.log(`Hello world!`);
};


const hello = () => {
	console.log(`Hello from arrow.`);
};

hello();

// function greet(pokemon1){
// 	console.log(`Hi, ${pokemon1.name1}!`)
// };
// greet(pokemon1)

const greet = (pokemon1) => {
	console.log(`Hi, ${pokemon1.name1}!`);
};
greet(pokemon1);

// Implicit Return - allows us to return a value without the use of return keyword.

// function addNum(num1, num2){
// 	console.log(num1 + num2);
// };

const addNum = (num1, num2) => num1 + num2;
let sum = addNum(5, 6);
console.log(sum);

// this keyword
let protagonist = {
	name: 'Cloud strife',
	occupation: 'Soldier',
	greet : function(){
		// traditional methods would have this keyword refer to the parent object
		console.log(this);
		console.log(`Hi! I'm ${this.name}.`)
	},
	introduceJob: () => {
		console.log(this);
		console.log(`I work as ${protagonist.occupation}.`);
	}
}

protagonist.greet();
protagonist.introduceJob();

// Class-base Object Blueprints
	//Classes are template of objects

	// Create a class
			//The constructor is a special method for creating and initializing an object
	function Pokemon(name, tyoe, level){
		this.name = name;
		this.type = type;
		this.level = level;
	};

	// ES6 Class Creation
	class Car {
		constructor(brand, name, year){
			this.brand = brand;
			this.name = name;
			this.year = year;
		}
	};

	let car1 = new Car('Toyota', 'Vios', '2002');
	console.log(car1);
	let car2 = new Car('Cooper', 'Mini', '1969');
	console.log(car2);
	let car3 = new Car('Porsche', '911', '1960');
	console.log(car3);
