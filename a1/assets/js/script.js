/*Activity:
1. Update and Debug the following codes to ES6
		Use template literals
		Use array/object destructuring
		Use arrow function
2. Create a class constructor able to receive 3 arguments
		- it should be able to receive two strings and a number
		- Using the this keyword assign properties:
			name,
			breed,
			dogAge = <7 * human years>
				- assign the parameters as values to each property.
3. Create 2 new objects using our class constructor
	This constructor should be able to create Dog objects.
	Log the 2 new Dog objects in the console or alert.*/

//Task1
  let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101", "Social Sciences 201"],
};

let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"],
};

const introduce = (student) => {

	//Note: You can destructure objects inside functions.

	console.log(`Hi! I'm ${student.name}. I am ${student.age} years old.`);
	console.log(`I study the following courses ${student.classes}.`);
}

introduce(student1);
introduce(student2);


const getCube = (num) => Math.pow(num,3)
	cube = getCube(3)
	console.log(cube);


let numArr = [15,16,32,21,21,2]

numArr.forEach(num =>
	console.log(num));

let numsSquared = numArr.map((num) => num ** 2);
	console.log(numsSquared);


//Task2


class DOGG {
	constructor(name, talent, hood){
		this.name = name;
		this.talent = talent;
		this.hood = hood;}
};
let Snoop = new DOGG('Snoop D.O.double.G', 'Rapping', 'WestCoast')
console.log(Snoop);
let Sen = new DOGG('Sen DoG', 'Rapping', 'Death Row Records')
console.log(Sen);
let Nate = new DOGG('Nate DoG', 'Rapping', 'Cypriss Hill')
console.log(Nate);
